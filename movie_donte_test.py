import unittest
from movie import Movie
from library import Library


class DonateMovieTest(unittest.TestCase):
    def setUp(self):
        self.movie = Movie()
        self.library = Library()
        self.library.donate(self.movie)

    def test_donate_movie(self):
        self.setUp()
        self.assertTrue(self.library.contains(self.movie))

    def test_copy_added(self):
        self.assertEqual(self.movie.get_copies(), 1)


if __name__ == '__main__':
    unittest.main()